import './assets/main.css'
import 'primevue/resources/themes/aura-light-green/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css';


import PrimeVue from 'primevue/config';

import {createApp} from 'vue';
import App from './App.vue'

const app = createApp(App);
app.use(PrimeVue);

app.mount('#app');

